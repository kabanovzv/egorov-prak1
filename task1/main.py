import os
import random
import sys

def generate_random_array(length):
    return [random.randint(1, 100) for _ in range(length)]

def print_array(array):
    for value in array:
        print(value)

if __name__ == "__main__":
    length_str = sys.argv[1]
    length = int(length_str)
    random_array = generate_random_array(length)
    print_array(random_array)
