import psycopg2
import time

time.sleep(20)


conn = psycopg2.connect(
    dbname="root",
    user="root",
    password="root",
    host="172.19.0.2",
    port="5432"
)

cur = conn.cursor()


cur.execute("""
    SELECT
        s.first_name || ' ' || s.last_name AS "ФИО",
        AVG(er.grade) AS "Средний балл"
    FROM
        Students s
    JOIN
        Exams e ON s.student_id = e.student_id
    JOIN
        ExamResults er ON e.exam_id = er.exam_id
    GROUP BY
        s.first_name, s.last_name;
""")

rows = cur.fetchall()


with open('result.txt', 'w') as file:
    file.write("ФИО\t\t\tСредний балл\n")
    for row in rows:
        file.write(f"{row[0]}\t{row[1]}\n")

cur.close()
conn.close()
