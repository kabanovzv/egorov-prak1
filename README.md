# Egorov prak1

 


 
Номер в списке – 14.
Репозиторий: https://gitlab.com/kabanovzv/egorov-prak1
 
Файл .gitlab-ci.yml является конфигурационным файлом для CI/CD в GitLab. Этот файл описывает автоматизированный процесс, который состоит из двух основных задач (stages): task1 и task2.
Задача 1 выполняет Python-скрипт и сохраняет его результаты, в то время как задача 2 управляет Docker-контейнерами, выполняет интеграционные тесты и извлекает необходимые логи и файлы.
 

Задание №1
Напишите две функции. Первая функция заполняет массив 
случайными значениями, вторая функция выводит массив на экран
Решение:
Создаем директорию task1, в нем создаем файл main.py
Данный код предназначен для генерации массива случайных чисел. Основная функциональность программы включает в себя создание массива с заданным количеством элементов, где каждый элемент является случайным числом в диапазоне от 1 до 100.
 
Переменные CI/CD в GitLab используются для хранения информации, которая может быть доступна в скриптах заданий (jobs) в процессе непрерывной интеграции и развертывания (CI/CD). Эти переменные предоставляют гибкий механизм для управления конфигурацией.
В данном случае переменная length использована для динамического определения размера массива Например, если length задана как 10, скрипт Python может использовать это значение для генерации массива из 10 случайных чисел. 

Задание №2
Вывод виде таблицы ФИО и среднеарифметическую оценку по всем 
предметам по зачетке

Создаем директорию task1, в нем создаем файлы main.py Dockerfile, docker- compose.yml, init.sql и main.py.


Dockerfile настроен для работы с Python-приложением, которое использует библиотеку psycopg2 для взаимодействия с базой данных PostgreSQL.


Docker-compose.yml используется для определения и запуска многоконтейнерных приложений Docker. В данном случае, он настраивает два сервиса: postgres для базы данных PostgreSQL и app для Python-приложения.

init.sql представляет собой SQL-скрипт, используемый для инициализации базы данных в контейнере Docker с PostgreSQL. Этот скрипт создает структуру таблиц и заполняет их начальными данными.

main.py содержит Python-скрипт, который подключается к базе данных PostgreSQL, выполняет SQL-запрос для получения среднего балла каждого студента и записывает результаты в файл result.txt.
 
После подтверждения работоспособности кода создается merge request (MR) в GitLab для переноса изменений из ветки dev в ветку main. 

